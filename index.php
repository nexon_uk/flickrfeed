<?php 
	//
	// This web application calls the Flickr public API to download and display
	// a set of Flickr posts.

	// Downloads are in json format. Currently json is the only format supported.
	// However the Flickr API offers many data formats. Extending the script
	// to handle other formats is a TODO.

	// The parameters to this app will pass on any valid Flickr API parameters. 
	// So a URL of http.../index.php?tags=snow  will download a batch of Flickr posts
	// with the tag "snow".

	// Modern browsers and IE9+ supported. jQuery v2 is used so there is no support 
	// for IE8 and earlier. 
	
	// This app has been tested on Safari 9.0, Firefox 43.0, Chrome 46.0.
	//
	
	header('Content-type:text/html; charset=utf-8');

	// filter for only valid flickr API options
	$flickrApiValidParams = [ 'id', 'ids', 'format', 'lang', 'tags', 'tagmode' ];
	$options = [];
	foreach ($flickrApiValidParams as $key)
	{
		if (isset($_REQUEST[$key]))
			$options[$key] = $_REQUEST[$key];
	}

	// default format option is json
	if (!array_key_exists('format', $options))
		$options['format'] = 'json';

	// build flickr request URL
	$paramSep = '?';
	foreach ($options as $k => $v)
	{
		$queryParams .= "${paramSep}${k}=${v}";
		$paramSep = '&';
	}

	// No provision (yet) for non-json formats!
	if ($options['format'] != 'json')
	{
		print "Sorry - ${options['format']} is not yet functional" . PHP_EOL;
		return;
	}

	// print $queryParams . PHP_EOL;
		
?>

<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>Flickr public feed</title>

<script src='https://code.jquery.com/jquery-2.1.4.min.js'></script>
<script>
	var url = 'https://api.flickr.com/services/feeds/photos_public.gne<?php echo $queryParams; ?>';

	$.getJSON(url + "&jsoncallback=?", function(data){
		var $container = $('#flickrPhotoStream');
		$.each(data.items, function(i,item){
			buildFlickrPost($container, data.items[i]);
		});
	});

function buildFlickrPost($container, item)
{
	$container.append('<div class="flickrPost">' + 
		"<img src=" + item.media.m + " />" + // image
		'<div class="title_and_credit">' + 
			"<a href=\"" + item.link + "\">" + item.title + "</a>" + // link to img
			" by " + 
			authorsFlickrUrl(item, 'credit') + 
		"</div>" + 
		postDescription(item) + 
		postTags(item) + 
		"</div>");
}

function authorsFlickrUrl(postHTML, urlClass)
{
	var $html = $(postHTML.description);
	var author = $html.find('a').eq(0).text();
	var url = $html.find('a').eq(0).attr('href');
	return '<a class="' + urlClass + '" href="' + url + '">' + author + '</a>';
}

function postDescription(postHTML)
{
	var descripHTML = '';
	if (postHTML.description)
	{
		var $html = $(postHTML.description);
		var descrip = $html.find('img').eq(0).attr('alt');
		descripHTML = '<p><span class="post_subheading">Description: </span>' + descrip + '</p>';
	}	
	return descripHTML;
}

function postTags(postHTML)
{
	var tagHTML = '';
	if (postHTML.tags)
		tagHTML =  '<p><span class="post_subheading">Tags: </span>' + postHTML.tags + '</p>';
	return tagHTML;
}

</script>

<!-- to minimise page load times place styles here -->
<style>
.flickrPost {
	margin:1em;
	max-width:260px;
	float:left;
	padding:0 0.6em;
	border:1px solid #000;
	overflow:hidden;
}

.flickrPost img {
	display:block;
	margin:.6em auto;
}

.flickrPost p {
	margin:0.5em 0;
}

.flickrPost a {
	color:blue;
	text-decoration:underline;
	font-size:15px;
}

.flickrPost .title_and_credit {
	margin:0.3em 0 0.5em 0;
	overflow:hidden;
	font-size:13px;
}

.flickrPost p {
	font-size:14px;
}

.flickrPost .post_subheading {
	font-weight:bold;
}
</style>
</head>

<body>
	<div id="flickrPhotoStream"> </div>
</body>
</html>
