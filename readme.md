Flickr Feed
============
This web application calls the Flickr public API to download and display a set of Flickr posts.

Downloads are in JSON format. Currently JSON is the only format supported.
However the Flickr API offers many data formats. Extending the script to handle other formats is a TODO.

The parameters to this app will pass on any valid Flickr API parameters.  So a URL of `http.../index.php?tags=snow`  will download a batch of Flickr posts with the tag "snow".

Support
--------
Modern browsers and IE9+ supported. jQuery v2 is used so there is no support for IE8 and earlier.  This app has been tested on Safari 9.0, Firefox 43.0, Chrome 46.0.
	
	
